package com.vadman.shooter.graphics;

import java.util.Random;

import com.vadman.shooter.Game;

public class Screen extends Render
{
	private Render test;
	private Render3D render;

	public Screen(int width, int height)
	{
		super(width, height);
		Random random = new Random();
		test = new Render(256, 256);
		render = new Render3D(width, height);
		for (int i = 0; i < test.drawwidth * test.drawheight; i++)
		{
			test.pixels[i] = random.nextInt();
		}
	}

	public void render(Game game)
	{
		for (int i = 0; i < width * height; i++)
		{
			pixels[i] = 0;
		}

		render.floor();
		draw(render, 0, 0);
	}
}
