package com.vadman.shooter.graphics;

import com.vadman.shooter.Display;

public class Render
{
	public final int width;
	public final int height;
	public int drawwidth;
	public int drawheight;
	public final int[] pixels;

	public Render(int width, int height)
	{
		drawwidth = 256;
		drawheight = 256;
		this.width = width;
		this.height = height;
		pixels = new int[width * height];
	}

	public void draw(Render render, int xOffset, int yOffset)
	{
		for (int y = 0; y < render.height; y++)
		{
			int yPix = y + yOffset;
			if (yPix < 0 || yPix >= Display.HEIGHT)
			{
				continue;
			}

			for (int x = 0; x < render.width; x++)
			{
				int xPix = x + xOffset;
				if (xPix < 0 || xPix >= Display.WIDTH)
				{
					continue;
				}
				//pixels[xPix + yPix * width] = render.pixels[x + y * render.width];
				
				int alpha = render.pixels[x + y * render.width];
				
				if (alpha > 0)
				{
					pixels[xPix + yPix * width] = alpha;
				}
			}
		}
	}
}
